#include <bits/types/struct_timespec.h>
#include <cstdlib>
#include <fmt/core.h>
#include <fmt/color.h>
#include <stdexcept>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#include "emulator.hpp"
#include "exception.hpp"
#include "sizes.h"

using namespace std;

Register::
Register(string n, uint8_t *addr): name(n), address(addr)
{ }

Emulator::
Emulator(uint8_t (&vm_code)[vm_code_size], uint8_t (&vm_mem)[vm_mem_size])
{
    /* copy over the given instructions */
    copy(begin(vm_code), end(vm_code), begin(this->vm_mem));

    /* copy over the vm memory */
    copy(begin(vm_mem), end(vm_mem), begin(this->vm_mem) + 768);

    /* populate the opcodes */
    opcodes.insert( pair<uint8_t, string> (0x00, "NONE") );
    opcodes.insert( pair<uint8_t, string> (0x01, "ldm") );
    opcodes.insert( pair<uint8_t, string> (0x02, "stk") );
    opcodes.insert( pair<uint8_t, string> (0x04, "add") );
    opcodes.insert( pair<uint8_t, string> (0x08, "jmp") );
    opcodes.insert( pair<uint8_t, string> (0x10, "sys") );
    opcodes.insert( pair<uint8_t, string> (0x20, "cmp") );
    opcodes.insert( pair<uint8_t, string> (0x40, "imm") );
    opcodes.insert( pair<uint8_t, string> (0x80, "stm") );

    /* populate the registers */
    registers.insert( pair<uint8_t, Register> (0x00, Register("NONE")) );
    registers.insert( pair<uint8_t, Register> (0x04, Register("a", &this->vm_mem[1024])) );
    registers.insert( pair<uint8_t, Register> (0x01, Register("b", &this->vm_mem[1025])) );
    registers.insert( pair<uint8_t, Register> (0x10, Register("c", &this->vm_mem[1026])) );
    registers.insert( pair<uint8_t, Register> (0x02, Register("d", &this->vm_mem[1027])) );
    registers.insert( pair<uint8_t, Register> (0x40, Register("s", &this->vm_mem[1028])) );
    registers.insert( pair<uint8_t, Register> (0x20, Register("i", &this->vm_mem[1029])) );
    registers.insert( pair<uint8_t, Register> (0x08, Register("f", &this->vm_mem[1030])) );

    /* populate the flag descriptions */
    flag_descriptions.insert( pair<uint8_t, char> (0x00, '*') );
    flag_descriptions.insert( pair<uint8_t, char> (0x01, 'G') );
    flag_descriptions.insert( pair<uint8_t, char> (0x02, 'E') );
    flag_descriptions.insert( pair<uint8_t, char> (0x04, 'L') );
    flag_descriptions.insert( pair<uint8_t, char> (0x08, 'G') );
    flag_descriptions.insert( pair<uint8_t, char> (0x10, 'N') );

    /* populate the syscalls */
    syscalls.insert( pair<uint8_t, string> (0x01, "exit") );
    syscalls.insert( pair<uint8_t, string> (0x02, "read_memory") );
    syscalls.insert( pair<uint8_t, string> (0x04, "write") );
    syscalls.insert( pair<uint8_t, string> (0x08, "open") );
    syscalls.insert( pair<uint8_t, string> (0x10, "read_code") );
    syscalls.insert( pair<uint8_t, string> (0x20, "sleep") );

    fmt::print(fmt::fg(fmt::terminal_color::bright_green),
            "[*] Emulator initialised!\n");

    return;
}

void Emulator::
interpret_instruction(uint8_t &opcode, uint8_t &arg1, uint8_t &arg2)
{
    /* describe_registers */
    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[V] a:{:#x} b:{:#x} c:{:#x} d:{:#x} s:{:#x} i:{:#x} f:{:#x}\n",
            vm_mem[1024], vm_mem[1025], vm_mem[1026],
            vm_mem[1027], vm_mem[1028], vm_mem[1029],
            vm_mem[1030]);

    /* describe instruction */
    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[I] op:{:#x} arg1:{:#x} arg2:{:#x}\n",
            opcode, arg1, arg2);

    string instruction = opcodes.find(opcode)->second;

    if ( instruction == "ldm" ) {
        interpret_ldm(arg1, arg2);
    }
    else if ( instruction == "stk" ) {
        interpret_stk(arg1, arg2);
    }
    else if ( instruction == "add" ) {
        interpret_add(arg1, arg2);
    }
    else if ( instruction == "jmp" ) {
        interpret_jmp(arg1, arg2);
    }
    else if ( instruction == "sys" ) {
        interpret_sys(arg1, arg2);
    }
    else if ( instruction == "cmp" ) {
        interpret_cmp(arg1, arg2);
    }
    else if ( instruction == "imm" ) {
        interpret_imm(arg1, arg2);
    }
    else if ( instruction == "stm" ) {
        interpret_stm(arg1, arg2);
    }
    else {
        throw std::logic_error("Invalid instruction!");
    }

    return;
}

void Emulator::
interpret_add(uint8_t &arg1, uint8_t &arg2)
{
    auto reg1 = registers.find( arg1 )->second;
    auto reg2 = registers.find( arg2 )->second;

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] ADD {} {}\n",
            reg1.name, reg2.name);

    *(reg1.address) += *(reg2.address);

    fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
            "[DEBUG] {} = {:#x}\n",
            reg1.name, *(reg1.address));

    return;
}

void Emulator::
interpret_cmp(uint8_t &arg1, uint8_t &arg2)
{
    auto reg1 = registers.find( arg1 )->second;
    auto reg2 = registers.find( arg2 )->second;

    auto reg1_val = *(reg1.address);
    auto reg2_val = *(reg2.address);

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] CMP {} {}\n",
            reg1.name, reg2.name);

    fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
            "[DEBUG] {} = {:#x}, {} = {:#x}\n",
            reg1.name, reg1_val,
            reg2.name, reg2_val);

    if (reg1_val > reg2_val) {
        vm_mem[1030] |= 1;
    }
    if (reg1_val == reg2_val) {
        vm_mem[1030] |= 2;
    }

    if (reg1_val < reg2_val) {
        vm_mem[1030] |= 4;
    }

    if ( !reg1_val && !reg2_val ) {
        vm_mem[1030] |= 8;
    }

    if (reg1_val != reg2_val) {
        vm_mem[1030] |= 0x10;
    }

    return;
}

void Emulator::
interpret_ldm(uint8_t &arg1, uint8_t &arg2)
{
    auto reg1 = registers.find( arg1 )->second;
    auto reg2 = registers.find( arg2 )->second;

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] LDM {} = *{}\n",
            reg1.name, reg2.name);

    auto reg_val = *(reg2.address);
    auto memory_val = read_memory(reg_val);

    *(reg1.address) = memory_val;

    fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
            "[DEBUG] LDM {} = {:#x} = vm_memory[{:d}]\n",
            reg1.name, *(reg1.address), reg_val+768);

    return;
}

void Emulator::
interpret_imm(uint8_t &arg1, uint8_t &arg2)
{
    Register reg = registers.find( arg1 )->second;

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] IMM {} = {:#x}\n",
            reg.name, arg2);

    *(reg.address) = arg2;
    return;
}

void Emulator::
interpret_stm(uint8_t &arg1, uint8_t &arg2)
{
    auto reg1 = registers.find( arg1 )->second;
    auto reg2 = registers.find( arg2 )->second;

    auto offset = *(reg1.address);
    auto value = *(reg2.address);

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] STM *{} = {}\n",
            reg1.name, reg2.name);

    fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
            "[DEBUG] vm_memory[{:d}] = {:#x}\n",
            offset + 768, value);

    write_memory(offset, value);
    return;
}

void Emulator::
interpret_jmp(uint8_t &arg1, uint8_t &arg2)
{
    auto flag = flag_descriptions.find(arg1);
    auto flag_opcode = flag->first;
    auto flag_description = flag->second;
    auto reg = registers.find( arg2 )->second;

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[j] JMP {} {}\n",
            flag_description, reg.name);

    if ( (flag_opcode & vm_mem[1030]) || (flag_description == '*') )
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_white),
                "[j] ... TAKEN\n");
        /* change the instruction pointer */
        vm_mem[1029] = *(reg.address);
    }
    else
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_white),
                "[j] ... NOT TAKEN\n");
    }

    return;
}

void Emulator::
interpret_stk(uint8_t &arg1, uint8_t &arg2)
{
    auto reg1 = registers.find( arg1 )->second;
    auto reg2 = registers.find( arg2 )->second;

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] STK {} {}\n",
            reg1.name, reg2.name);

    if (reg2.name != "NONE")
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_white),
                "[s] ... pushing {}\n", reg2.name);

        ++vm_mem[1028];
        write_memory(vm_mem[1028], *(reg2.address));
    }

    if (reg1.name != "NONE")
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_white),
                "[s] ... popping {}\n", reg1.name);

        auto value = read_memory(vm_mem[1028]);
        *(reg1.address) = value;
        --vm_mem[1028];
    }
    return;
}

void Emulator::
interpret_sys(uint8_t &syscall_opcode, uint8_t &return_register_opcode)
{
    auto syscall = syscalls.find( syscall_opcode )->second;
    auto return_register = registers.find( return_register_opcode )->second;

    auto reg_a = vm_mem[1024];
    auto reg_b = vm_mem[1025];
    auto reg_c = vm_mem[1026];

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] SYS {:#x} {}\n",
            syscall_opcode, return_register.name);

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] ... {}\n",
            syscall);

    if ( syscall == "open" )
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
                "[DEBUG] {}(&vm_mem[{:d}], {:d}, {:d})\n",
                syscall, reg_a+768, reg_b, reg_c);

        *(return_register.address) = open((const char *)&vm_mem[reg_a+768], reg_b, reg_c);
    }
    else if ( syscall == "read_code" )
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
                "[DEBUG] {}({:d}, &vm_mem[{:d}], {:d})",
                syscall, reg_a, 3*reg_b, reg_c);

        fmt::print(fmt::fg(fmt::terminal_color::bright_white), "\n");

        *(return_register.address) = read(reg_a, &vm_mem[3*reg_b], reg_c);
    }
    else if ( syscall == "read_memory" )
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
                "[DEBUG] {}({:d}, &vm_mem[{:d}], {:d})",
                syscall, reg_a, reg_b+768, reg_c);

        fmt::print(fmt::fg(fmt::terminal_color::bright_white), "\n");

        *(return_register.address) = read(reg_a, &vm_mem[reg_b + 768], reg_c);
    }
    else if ( syscall == "write" )
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
                "[DEBUG] {}({:d}, &vm_mem[{:d}], {:d})",
                syscall, reg_a, reg_b+768, reg_c);

        fmt::print(fmt::fg(fmt::terminal_color::bright_white), "\n");

        *(return_register.address) = write(reg_a, &vm_mem[reg_b + 768], reg_c);
    }
    else if ( syscall == "sleep" )
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
                "[DEBUG] {}({:d})\n",
                syscall, reg_a);

        struct timespec tm;
        tm.tv_sec = reg_a;
        tm.tv_nsec = 0;

        *(return_register.address) = nanosleep(&tm, NULL);
    }
    else if ( syscall == "exit" )
    {
        fmt::print(fmt::fg(fmt::terminal_color::bright_cyan),
                "[DEBUG] {}({:d})\n",
                syscall, reg_a);
        exit(reg_a);
    }

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[s] ... return value (in register {}): {:#x}\n",
            return_register.name, *(return_register.address));

    return;
}

void Emulator::
interpreter_loop(void)
{
    uint8_t opcode;
    uint8_t arg1;
    uint8_t arg2;
    uint8_t ip;

    fmt::print(fmt::fg(fmt::terminal_color::bright_white),
            "[+] Starting interpreter loop!\n");

    /* loop over the vm code 3-bytes chunk at a time.
     * In the chunk, the first byte is the operation
     * the third byte is arg1 and the second byte is arg2
     */
    for ( ;; )
    {
        /* increment the ip after executing each instruction */
        ip = vm_mem[1029];
        vm_mem[1029] = ip + 1;

        opcode = vm_mem[3*ip];
        arg1 = vm_mem[3*ip+2];
        arg2 = vm_mem[3*ip+1];

        try {
            interpret_instruction(opcode, arg1, arg2);
        }
        catch(...) {
            process_exception();
            exit(EXIT_FAILURE);
        }
    }
    return;
}

uint8_t Emulator::
read_memory(uint8_t &offset)
{
    return vm_mem[offset + 768];
}

void Emulator::
write_memory(uint8_t &offset, uint8_t &value)
{
    vm_mem[offset + 768] = value;
    return;
}
