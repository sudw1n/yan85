#pragma once
#include <string>
#include <map>
#include "sizes.h"

using namespace std;

class Register
{
    public:
        string name;
        uint8_t *address;

        Register(string, uint8_t * = NULL);
};

/* the Yan85 emulator */
class Emulator
{
    protected:
        /* the mapping of instructions to their opcodes */
        map<uint8_t, string> opcodes;
        /* the mapping of register opcodes to their names */
        map<uint8_t, Register> registers;
        /* the mapping of flag opcodes to their character descriptions */
        map<uint8_t, char> flag_descriptions;
        /* the mapping of syscall opcodes */
        map<uint8_t, string> syscalls;

        /* address space for the vm */
        uint8_t vm_mem[1040] = { };

    private:
        /* private methods */
        string describe_register(uint8_t &reg);
        char describe_flags(uint8_t &reg);
        void interpret_instruction(uint8_t &, uint8_t &, uint8_t &);
        void interpret_add(uint8_t &, uint8_t &);
        void interpret_cmp(uint8_t &, uint8_t &);
        void interpret_ldm(uint8_t &, uint8_t &);
        void interpret_imm(uint8_t &, uint8_t &);
        void interpret_stm(uint8_t &, uint8_t &);
        void interpret_jmp(uint8_t &, uint8_t &);
        void interpret_stk(uint8_t &, uint8_t &);
        void interpret_sys(uint8_t &, uint8_t &);
        inline uint8_t read_memory(uint8_t &);
        inline void write_memory(uint8_t &, uint8_t &);
    public:
        Emulator(void);
        Emulator(uint8_t (&)[vm_code_size], uint8_t (&)[vm_mem_size]);
        void interpreter_loop(void);
};
