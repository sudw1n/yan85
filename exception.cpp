#include <exception>
#include <ios>
#include <stdexcept>
#include <system_error>
#include <future>
#include <iostream>

template<typename T> void
process_code_exception (const T& e)
{
    using namespace std;
    auto c = e.code();
    cerr << "- category:            " << c.category().name() << "\n";
    cerr << "- value:               " << c.value() << "\n";
    cerr << "- message:             " << c.message() << "\n";
    cerr << "- default\n  category: "
         << c.default_error_condition().category().name() << endl;
    cerr << "- def value:           "
         << c.default_error_condition().value() << "\n";
    cerr << "- def msg:             "
         << c.default_error_condition().message() << "\n";
}

void
process_exception()
{
    using namespace std;
    try {
        throw; // rethrow exception to deal here
    }
    catch(const ios_base::failure& e) {
        cerr << "I/O EXCEPTION: " << e.what() << endl; process_code_exception(e);
        process_code_exception(e);
    }
    catch (const system_error& e) {
        cerr << "SYSTEM EXCEPTION: " << e.what() << endl;
        process_code_exception(e);
    }
    catch (const future_error& e) {
        cerr << "FUTURE EXCEPTION: " << e.what() << endl;
        process_code_exception(e);
    }
    catch (const bad_alloc& e) {
        cerr << "BAD ALLOC EXCEPTION: " << e.what() << endl;
    }
    catch (const logic_error& e) {
        cerr << "LOGICAL EXCEPTION: " << e.what() << endl;
    }
    catch (const exception& e) {
        cerr << "EXCEPTION: " << e.what() << endl;
    }
    catch (...) {
        cerr << "EXCEPTION (unknown)" << endl;
    }
}
