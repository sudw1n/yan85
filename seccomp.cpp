#include <seccomp.h>
#include <fmt/core.h>
#include <fmt/color.h>
#include <stdexcept>
#include "seccomp.hpp"
#include "exception.hpp"


void
filter_syscalls(void)
{
    /* initialise the seccomp filter state */
    auto ctx = seccomp_init(SCMP_ACT_KILL_PROCESS);

    fmt::print(fmt::fg(fmt::terminal_color::bright_magenta),
            "[*] Initialising seccomp\n");

    if (ctx == NULL)
        goto out;

    /* apply filters */
    fmt::print(fmt::fg(fmt::terminal_color::bright_magenta),
            "[*] Allowing open()\n");
    if (seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(open), 0) < 0)
        goto out;
    if (seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(openat), 0) < 0)
        goto out;

    fmt::print(fmt::fg(fmt::terminal_color::bright_magenta),
            "[*] Allowing read()\n");
    if (seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(read), 0) < 0)
        goto out;

    fmt::print(fmt::fg(fmt::terminal_color::bright_magenta),
            "[*] Allowing write()\n");
    if (seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(write), 0) < 0)
        goto out;

    fmt::print(fmt::fg(fmt::terminal_color::bright_magenta),
            "[*] Allowing nanosleep()\n");
    if (seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(nanosleep), 0) < 0)
        goto out;

    fmt::print(fmt::fg(fmt::terminal_color::bright_magenta),
            "[*] Allowing exit()\n");
    if (seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit), 0) < 0)
        goto out;
    if (seccomp_rule_add(ctx, SCMP_ACT_ALLOW, SCMP_SYS(exit_group), 0) < 0)
        goto out;

    fmt::print(fmt::fg(fmt::terminal_color::bright_magenta),
            "[*] Logging futex()\n");
    if (seccomp_rule_add(ctx, SCMP_ACT_LOG, SCMP_SYS(futex), 0) < 0)
        goto out;

    /* load and enforce the filters */
    if ( seccomp_load(ctx) < 0)
        goto out;

    fmt::print(fmt::fg(fmt::terminal_color::bright_magenta),
            "[*] Successfully applied filters!\n");
    return;

out:
    seccomp_release(ctx);
    throw std::logic_error("Failed to apply seccomp filters");
}
