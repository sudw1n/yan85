#pragma once

/* display the details of the exception */
template<typename T>
void process_code_exception (const T& e);
/* catch-all exception handling routine */
void process_exception(void);
