.PHONY : yan85

CFLAGS = -std=c++20 -O3 -flto
CC = clang++
LDFLAGS = -fuse-ld=mold -s -lfmt -lseccomp

yan85: main.o emulator.o exception.o seccomp.o
	$(CC) $(CFLAGS) $(LDFLAGS) -o yan85 main.o emulator.o exception.o seccomp.o

main.o: main.cpp
	$(CC) $(CFLAGS) -c main.cpp

emulator.o: emulator.cpp emulator.hpp
	$(CC) $(CFLAGS) -c emulator.cpp

exception.o: exception.cpp exception.hpp
	$(CC) $(CFLAGS) -c exception.cpp

seccomp.o: seccomp.cpp seccomp.hpp
	$(CC) $(CFLAGS) -c seccomp.cpp

clean:
	rm -f *.o
