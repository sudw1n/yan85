# Yan85
Yan85 is a custom architecture used in the CTF challenges from [pwn.college](https://pwn.college/). I made this one while
doing the Reverse Engineering (`babyrev 16` onwards) module.

This emulator additionally also uses [libseccomp](https://github.com/seccomp/libseccomp) to filter system
calls that the emulator can use. This is completely unnecessary and I only put it there because I
wanted to.

## Usage
In order for the emulator to start working, it needs to have the `vm_code` array inside `main.cpp`
populated with _Yan85_ opcodes. Since, I'm a terrible programmer I also haven't figured out a way to
use a better container for the emulator's address space. Therefore, we also need to modify the
`sizes.h` file according to the populated arrays. Compiling the source as is won't make the emulator
work.

![Emulator doesn't work without any instructions](./emulator-initial.png)

In addition to `libseccomp`, I have also used the [{fmt}](https://fmt.dev) library for formatting my
code and make the output look pretty so we need to have `libfmt` installed as well.

Once that's done, compile with the following command-line:
```
make -j <cores>
```

Here, `cores` is the number of CPU cores to be used by make. The executable will be named `yan85`.
