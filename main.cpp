#include <cstdlib>

#include "emulator.hpp"
#include "exception.hpp"
#include "seccomp.hpp"

#include "sizes.h"

int
main(void)
{
    /* initialise seccomp and apply filters */
    try {
        filter_syscalls();
    }
    catch (...)
    {
        process_exception();
        exit(EXIT_FAILURE);
    }

    /* instructions for the emulator */
    uint8_t vm_code[vm_code_size]{ 0 };
    uint8_t vm_mem[vm_mem_size]{ 0 };

    auto yan85 = new Emulator(vm_code, vm_mem);

    /* start executing instructions */
    yan85->interpreter_loop();

    delete yan85;
    return 0;
}
